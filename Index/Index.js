/*
################################################################################
# Synclair                                                                     #
# Copyright (C) 2011-2022 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################
*/

async function OnSuccess(Item) {
  var Parser = new DOMParser();
  var Html = Parser.parseFromString(Item.Html, "text/html");
  document.head.appendChild(Html.head);
  document.body.appendChild(Html.body);
  var Style =
`<style>
 ${Item.Css}
 </style>`;
  for (var i = 0; i < Item.SheetNumber; i++) {
    try {
      var LinkSheet = JSON.parse(Item[i]);
    } catch (e) {
    }
    if (LinkSheet) {
      for (let Key of Object.keys(LinkSheet)) {
        var RegText = `"${Key}"`;
        var Regex =  new RegExp(RegText,'g');
        Style = Style.replace(Regex, `"${LinkSheet[Key]}"`);
        var ToReplace = document.getElementsByClassName(Key);
        for (var j = 0; j < ToReplace.length; j++) {
          ToReplace[j].setAttribute("cite", LinkSheet[Key]);
          ToReplace[j].setAttribute("data", LinkSheet[Key]);
          ToReplace[j].setAttribute("href", LinkSheet[Key]);
          ToReplace[j].setAttribute("src", LinkSheet[Key]);
        }
      }
    }
  }
  document.head.appendChild(Parser.parseFromString(Style, "text/html").head);
}
browser.storage.sync.get().then(OnSuccess);
