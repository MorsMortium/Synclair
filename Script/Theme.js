/*
################################################################################
# Synclair                                                                     #
# Copyright (C) 2011-2022 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################
*/

async function GetTheme() {
    SetStyle(await browser.theme.getCurrent());
}

function SetStyle(Theme) {
    if (Theme.colors) {
        var Parser = new DOMParser();

        Color = Theme.colors.popup_text;
        if (Color.toString() == "hsl(0, 0%, 100%)") {
            Color = "rgb(255, 255, 255)";
        }
        if (Color.toString() == "hsl(0, 0%, 0%)") {
            Color = "rgb(0, 0, 0)";
        }
        BackgroundColor = "rgba" + Color.slice(3, Color.length - 1) + ", .17)"

        var Style = Parser.parseFromString(`
        <style>
        * {
            color: ${Color};
            background-color: ${Theme.colors.popup};
        }
        button:hover, input:hover {
            background-color: ${BackgroundColor};
        }
        </style>`, "text/html");
        document.head.appendChild(Style.head);
    }
}

GetTheme();
