/*
################################################################################
# Synclair                                                                     #
# Copyright (C) 2011-2022 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################
*/

function OnInstall() {
    function OnSuccess(Item) {
        if (!Item.Html) {
            browser.storage.sync.set({Html:
    `<title>Synclair</title>
<h1>Tutorial</h1>
<ul>
    <li>Click on menu icon, choose Html/Css/Open sheet option, to open editor.</li>
    <li>After editing click Save then close the tab.</li>
    <li>In the html file, tags with classes will get their cite/data/href/src attributes replaced by the links of same name in the link sheet.</li>
    <li>In the css file, every mention of the link sheet names, between quotes will be replaced by the links.</li>
    <li>You can choose which link sheet to open or remove, but add always adds the new link sheet to the end of the list.</li>
    <li>If a link name exists more than one time, the last one will be used.</li>
    <li>Links and link names have to be between double quotes, on the link sheet.</li>
    <li>To test while editing, save and either open a new tab, or click on the menu icon then on Open Synclair.</li>
    <li>You can hard code links, but there is an 8kb limit on the size of both the html and the css text.</li>
    <li>After finishing, you can hide the icon, or put it in the overflow menu, so there will not be any accidental removals.</li>
    <li>If you like the extension consider donating and/or sending me tips on how to improve it.</li>
    <li><a class="support">https://morsmortium.codeberg.page/</a>
</ul>
<div><img class="example"></div>
<div id="example">css example</div>`});
        }

        if (!Item.Css) {
            browser.storage.sync.set({Css:
    `html {
    background: linear-gradient(to top right, rgb(255, 0, 0), rgb(0, 255, 0), rgb(0, 0, 255));
    height:100%;
}
img {
    height: 200px;
    width: 200px;
}
#example {
    background-image: url("example");
    background-size: 200px 200px;
    height: 200px;
    width: 200px;
    display: block;
}
div {
    float: left;
}`});
        }
        if (!Item.SheetNumber) {
            browser.storage.sync.set({0:
    `{
    "example": "https://upload.wikimedia.org/wikipedia/commons/1/1b/Example_jpg_high_quality.jpg",
    "support": "https://morsmortium.codeberg.page/"
}`});
            browser.storage.sync.set({SheetNumber: 1});
        }
    }
        browser.storage.sync.get().then(OnSuccess);
    }

browser.runtime.onInstalled.addListener(OnInstall);
