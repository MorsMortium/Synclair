/*
################################################################################
# Synclair                                                                     #
# Copyright (C) 2011-2022 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################
*/

function OnSuccess(Item) {
  var Text = document.getElementById("Text");
  Text.value = Item[Item.LastItem];
  document.title = browser.i18n.getMessage("EditFile") + Item.LastItem;
  function OnFailure() {
    window.alert(browser.i18n.getMessage(
      "SizeError", JSON.stringify(Text.value).length + SaveButton.name.length));
  }
  function SaveFunction(evt) {
    browser.storage.sync.set({[evt.currentTarget.name]: Text.value})
    .then(undefined, OnFailure);
  }
  var SaveButton = document.getElementById("SaveButton");
  SaveButton.textContent = browser.i18n.getMessage("SaveFile");
  SaveButton.addEventListener("click", SaveFunction);
  SaveButton.name = Item.LastItem;
}
browser.storage.sync.get().then(OnSuccess);
