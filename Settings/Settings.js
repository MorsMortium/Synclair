/*
################################################################################
# Synclair                                                                     #
# Copyright (C) 2011-2022 MorsMortium                                          #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################
*/

var WhichSheet = document.getElementById("WhichSheet");

function OpenSynclair() {
  browser.tabs.create({"url": "/Index/Index.html"});
}

function SheetOpen() {
  var SheetNumber = parseInt(WhichSheet.value);
  function OnSuccess(item) {
    if (0 <= SheetNumber && SheetNumber < item.SheetNumber) {
      browser.storage.sync.set({LastItem: WhichSheet.value});
      browser.tabs.create({"url": "/Editor/Editor.html"});
    }
  }
  browser.storage.sync.get().then(OnSuccess);
}

function UpdateNumber() {
  function OnSuccess(Item) {
    WhichSheet.setAttribute("max", Item.SheetNumber - 1);
  }
  browser.storage.sync.get().then(OnSuccess);
}

function SheetRemove() {
  var SheetNumber = parseInt(WhichSheet.value);
  function OnSuccess(Item) {
    for (var i = SheetNumber; i < Item.SheetNumber; i++) {
      browser.storage.sync.set({[i]: Item[i + 1]});
    }
    browser.storage.sync.set({SheetNumber: Item.SheetNumber - 1});
    UpdateNumber();
    WhichSheet.value = 0;
  }
  if (window.confirm(browser.i18n.getMessage("RemoveQuestion"))) {
    browser.storage.sync.get().then(OnSuccess);
  }
}

function SheetAdd() {
  function OnSuccess(Item) {
    browser.storage.sync.set({[Item.SheetNumber]:
`{
 "example": "https://upload.wikimedia.org/wikipedia/commons/1/1b/Example_jpg_high_quality.jpg",
 "support": "https://MorsMortium.codeberg.page"
}`});
    browser.storage.sync.set({SheetNumber: Item.SheetNumber + 1});
    UpdateNumber();
    WhichSheet.value = 0;
  }
  browser.storage.sync.get().then(OnSuccess);
}

function EditorSite(Event) {
  browser.storage.sync.set({LastItem: Event.currentTarget.name});
  browser.tabs.create({"url": "/Editor/Editor.html"});
}

function AboutSite(Event) {
  browser.tabs.create({"url": "https://MorsMortium.codeberg.page"});
}


var ButtonList = [
  ["OpenButton", OpenSynclair],
  ["OpenSheet", SheetOpen],
  ["WhichSheet", UpdateNumber],
  ["RemoveSheet", SheetRemove],
  ["AddSheet", SheetAdd],
  ["Html", EditorSite],
  ["Css", EditorSite],
  ["AboutButton", AboutSite]
];

for (var i = 0; i < ButtonList.length; i++) {
  var ButtonOne = document.getElementById(ButtonList[i][0]);
  ButtonOne.textContent = browser.i18n.getMessage(ButtonList[i][0]);
  ButtonOne.addEventListener("click", ButtonList[i][1]);
  ButtonOne.name = ButtonList[i][0];
}

UpdateNumber();
